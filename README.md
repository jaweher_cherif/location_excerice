
- Run the server: 
    cd .\backend\
    node index
- Run the Client:
    cd .\frontend
    npm start

- Demo: https://recordit.co/jlzLSVmjX4?fbclid=IwAR18mpKkgSy6dYrfX-3vwRh9L94fMXRm09UdfWTY-TnGJWGklWZ2taDooYU
